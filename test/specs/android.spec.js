describe( 'Android findelements tests',() => {
    it( 'Find element by accessibility ID',async () => {

        // find element 
        
        await driver.pause(5000);
       //const loginBtn= await $('~com.planday.ninetofiveapp.stage:id/btLogin');
       const loginBtn= await $('//*[@resource-id="com.planday.ninetofiveapp.stage:id/btLogin"]');
                                 //com.planday.ninetofiveapp.stage:id/btLogin
                                // $('//*[@content-desc="helpme"]')

        // click 
        await loginBtn.waitForDisplayed();
       await loginBtn.click();                                                                                              
        //assert
       
        const portalTitle = await $('//*[@resource-id="com.planday.ninetofiveapp.stage:id/btLogin"]');
        await expect(portalTitle).toBeExisting();

    //await driver.pause(8000);
    
    })


    it.only( 'Find element by classname',async () => {

        await driver.pause(2000);
       
       const loginBtnClass= await $('android.widget.Button');
                                
        await loginBtnClass.waitForDisplayed();

      
        console.log(await loginBtnClass.getText());
        //assert
      
        await expect(loginBtnClass).toHaveText("LOG IN");

    //await driver.pause(8000);
    
    });

});